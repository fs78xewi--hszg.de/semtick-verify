from flask import Flask
from flask_babel import Babel
from flask_sqlalchemy import SQLAlchemy
import config

db = SQLAlchemy()
babel = Babel()

import app.models as models


def create_app():
    app = Flask(__name__)
    app.secret_key = "43ctr5zt78ueddsfui"
    app.url_map.strict_slashes = False

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = config.DATABASE_URI

    db.init_app(app)
    with app.app_context():
        db.create_all()
        __create_initial_db_entries()
        db.session.commit()

    babel.init_app(app)

    from .client import client as client_blueprint
    app.register_blueprint(client_blueprint)

    return app


def __create_initial_db_entries():
    from app.models import Certificate

    if Certificate.query.count() != 0:
        return

from flask import render_template, request, flash
from flask_babel import lazy_gettext as _l

import config
from . import client
from .. import babel
from ..models import Certificate


#
# Locale Initialization
#

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(config.LANGUAGES)


#
# Routes
#

@client.route("/", methods=["GET", "POST"])
def index():
    if "supply_btn" in request.form and request.method == "POST":
        input_verify_code = request.form["verify_code"]

        verify_code = Certificate.query.filter_by(verify_code=input_verify_code).first()
        if verify_code is not None:
            return render_template("/certificate.html",
                                   first_name=verify_code.first_name,
                                   last_name=verify_code.last_name,
                                   matricula_number=verify_code.matricula_number,
                                   issue_date=verify_code.issue_date,
                                   expiration_date=verify_code.expiration_date,
                                   description=verify_code.description,
                                   approved_by=verify_code.approved_by)

        flash(_l("Invalid Verify Code!"))

    return render_template("/input.html")

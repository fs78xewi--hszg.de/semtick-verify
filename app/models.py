from . import db


class Certificate(db.Model):
    verify_code = db.Column(db.String(11), primary_key=True)
    first_name = db.Column(db.String(50), unique=False, nullable=False)
    last_name = db.Column(db.String(200), unique=False, nullable=False)
    matricula_number = db.Column(db.Integer, unique=False, nullable=False)
    issue_date = db.Column(db.DateTime, unique=False, nullable=False)
    expiration_date = db.Column(db.DateTime, unique=False, nullable=False)
    description = db.Column(db.String(500), unique=False, nullable=True)
    approved_by = db.Column(db.String(200), unique=False, nullable=False)

    def __init__(self, first_name, last_name, matricula_number,
                 issue_date, expiration_date, description, approved_by):
        self.first_name = first_name
        self.last_name = last_name
        self.matricula_number = matricula_number
        self.issue_date = issue_date
        self.expiration_date = expiration_date
        self.description = description
        self.approved_by = approved_by

    def __repr__(self):
        return "<Certificate %r>" % self.verify_code

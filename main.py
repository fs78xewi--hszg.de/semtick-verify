import logging

from app import create_app


def __setup_logging():
    logging.basicConfig(format="[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S %z", level=logging.INFO)


def shutdown():
    logging.info("Power off.")


def startup():
    logging.info("Powered on.")


__setup_logging()
app = create_app()
